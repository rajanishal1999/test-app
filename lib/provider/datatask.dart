import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:todooo/dbhelper/db_helper.dart';
import 'package:todooo/models/task.dart';
import 'dart:io';




class TaskData with ChangeNotifier{

  final dbHelper = DatabaseHelper.instance;

  List<Task> _items = [
    // Task(id: DateTime.now().toIso8601String(), todoname: 'buy milk', todotime: DateTime.now()),
  ];


  List<Task> get getItems {
    return _items;
  }


  void addtodo(String note) async {
    Task newtask = Task(id: DateTime.now().toIso8601String(), todoname: note, todotime: DateTime.now());
    _items.add(newtask);
    //notifyListeners();
    Map<String, dynamic> row = {
      DatabaseHelper.storeid : newtask.id,
      DatabaseHelper.storetodoname : newtask.todoname,
      DatabaseHelper.storetodotime  : newtask.todotime.toIso8601String()
    };
    final id = await dbHelper.insert(row);
    print('inserted row id: $id');

  }

  void update(String id, String newtitle) async {
    int prevoiusindex = _items.indexWhere((element) => element.id == id);
    _items.removeAt(prevoiusindex);
    Task newtask = Task(id: DateTime.now().toIso8601String(), todoname: newtitle, todotime: DateTime.now());
    _items.add(newtask);
    notifyListeners();

    Task updatetodo = Task(id: _items[prevoiusindex].id, todoname: newtitle, todotime: DateTime.now());
    final rowsAffected = await dbHelper.update({'id':updatetodo.id, 'todoname' :updatetodo.todoname, 'todotime':updatetodo.todotime});
    print('updated $rowsAffected row(s)');

  }


  void delete(String id) async{
    int prevoiusindex = _items.indexWhere((element) => element.id == id);
    _items.removeAt(prevoiusindex);
    notifyListeners();

    // final rowsDeleted = await dbHelper.delete(prevoiusindex + 1);
    // print('deleted $rowsDeleted row(s): row $id');

  }


  Future<void> fetchandsertplaces() async {
    final allRows = await dbHelper.queryAllRows();
    print('query all rows:');
    print(DateTime.now().toIso8601String());
    _items = allRows.map((element) => Task(id: element['id'], todoname: element['todoname'] , todotime: DateTime.parse(element['todotime']))).toList();
    notifyListeners();
  }





}