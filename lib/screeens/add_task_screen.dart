import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todooo/provider/datatask.dart';


enum NoteType {
  add, edit
}

class AddTaskScreen extends StatelessWidget {
  static String routename = 'addtaskscreen';

  final NoteType notetype;
  final String id;
  final String title;
  AddTaskScreen({this.notetype, this.id, this.title});

  TextEditingController _textcontroller = TextEditingController();

  initmethod(){
    if(NoteType.edit == notetype) {
      _textcontroller.text = title;
    }
  }

  @override
  Widget build(BuildContext context) {
    initmethod();
    return Scaffold(
      appBar: AppBar(
        title: Text(
          notetype == NoteType.add ? 'Add Todo' : 'Edit todo'
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save, color: Colors.lightBlueAccent,),
            onPressed: () {
              if(_textcontroller.text.isEmpty) {return;}
              if(NoteType.add == notetype) {
                Provider.of<TaskData>(context, listen: false).addtodo(_textcontroller.text);
              }else {
                Provider.of<TaskData>(context, listen: false).update(id, _textcontroller.text);
              }
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TextField(
              decoration: InputDecoration(labelText: notetype == NoteType.add ? 'Add Todo' : 'Edit todo', labelStyle: TextStyle(color: Colors.lightBlueAccent),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.lightBlueAccent),
                ),
              ),
              controller: _textcontroller,
              keyboardType: TextInputType.text,
            ),
          ],
        ),
      ),
    );
  }
}
