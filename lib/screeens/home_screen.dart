import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todooo/provider/datatask.dart';
import 'package:todooo/screeens/add_task_screen.dart';
import 'package:todooo/widgets/task_item.dart';





class HomeScreen extends StatelessWidget {



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Todo App'),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddTaskScreen(notetype: NoteType.add),
              ));
        },
      ),
      body: FutureBuilder(
        future: Provider.of<TaskData>(context, listen: false).fetchandsertplaces(),
        builder: (ctx, snapshot) => snapshot.connectionState == ConnectionState.waiting
          ? Center(child: CircularProgressIndicator(),)
          : Consumer<TaskData>(
          builder: (ctx, taskdata, chd) => taskdata.getItems.length == 0 ? chd : ListView.builder(
            itemBuilder: (ctx, index) => TaskItem(
              todotime: taskdata.getItems[index].todotime,
              todoname: taskdata.getItems[index].todoname,
              todoid: taskdata.getItems[index].id,
            ),
            itemCount: taskdata.getItems.length,
          ),
          child: Center(child: Text('No Items', style: TextStyle(fontSize: 20),),),
        ),
      ),
    );
  }


}

