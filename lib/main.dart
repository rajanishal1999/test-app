import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todooo/provider/datatask.dart';
import 'package:todooo/screeens/add_task_screen.dart';
import 'package:todooo/screeens/home_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: TaskData(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: Colors.red
        ),
        home: HomeScreen(),
        routes: {
          AddTaskScreen.routename : (ctx) => AddTaskScreen(),
        },
      ),
    );
  }
}





