import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:todooo/provider/datatask.dart';
import 'package:todooo/screeens/add_task_screen.dart';



class TaskItem extends StatelessWidget {


  final String todoname;
  final DateTime todotime;
  final String todoid;


  TaskItem({this.todoname, this.todotime, this.todoid});



  void deletemethod(BuildContext ctx) {
    Provider.of<TaskData>(ctx).delete(todoid);
  }

  @override
  Widget build(BuildContext context) {
    final devicesize = MediaQuery.of(context).size;
    return Dismissible(
      key: Key(UniqueKey().toString()),
      //key: ValueKey(todoid),
      direction: DismissDirection.endToStart,
      background: Container(
        color: Colors.red,
        child: Icon(Icons.delete, color: Colors.white, size: 40,),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 20),
      ),
      confirmDismiss: (direction) {
        return showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Delete'),
            content: Text('are you sure you want to delete'),
            actions: <Widget>[
              FlatButton(
                child: Text('No'),
                onPressed: () {
                  Navigator.of(ctx).pop(false);
                },
              ),
              FlatButton(
                child: Text('Yes'),
                onPressed: () {
                  Navigator.of(ctx).pop(true);
                },
              ),
            ],
          ),
        );
      },
      onDismissed: (diection) {
        Provider.of<TaskData>(context, listen: false).delete(todoid);
      },
      child: Container(
        height: devicesize.height / 7,
        width: devicesize.width,
        padding: EdgeInsets.only(top: 4, bottom: 4, right: 12, left: 12),
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text(
                todoname,
                style: TextStyle(fontSize: 20),
              ),
              subtitle: Text(DateFormat('dd-MM-yy – HH:mm').format(todotime)),
              trailing: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AddTaskScreen(notetype: NoteType.edit, id: todoid, title: todoname,),
                      ));
                  },
                child: Icon(
                  Icons.edit, color: Colors.blueAccent,
                ),
              ),
            ),
            Divider(),
          ],
        ),
      ),
    );
  }
}
